import React, { Component } from 'react';
import { Bar } from 'react-chartjs-2';
import { Line } from 'react-chartjs-2';
import { Doughnut } from 'react-chartjs-2';
import { HorizontalBar } from 'react-chartjs-2';
import { Pie } from 'react-chartjs-2';
import Data1 from "./Data1"; 

const socialMediaList = Data1;
const data = {
    labels:  socialMediaList.Names ,
    datasets: [
        {
            label: 'My First dataset',
            backgroundColor: '#0097d4',
            borderColor: '#0072a0',
            borderWidth: 1,
            hoverBackgroundColor: '#0072a0',
            hoverBorderColor: 'rgba(255,99,132,1)',
            data: socialMediaList.Numbers
        }
    ]
};


const data1 = {
    labels: socialMediaList.Names,
    datasets: [
        {
            label: 'My First dataset',
            fill: false,
            lineTension: 0.1,
            backgroundColor: '#0097d4',
            borderColor: '#0072a0',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: 'rgba(75,192,192,1)',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'rgba(75,192,192,1)',
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: socialMediaList.Numbers
        }
    ]
};


const data2 = {
    labels: socialMediaList.Names,   
    datasets: [{
        data: socialMediaList.Numbers,
        backgroundColor: [
            '#58508d',
            '#bc5090',
            '#ff6361',
            '#003f5c'
        ],
        hoverBackgroundColor: [
            '#58508d',
            '#bc5090',
            '#ff6361',
            '#003f5c'
        ]
    }]
};

const data3 = {
    labels: socialMediaList.Names ,
    datasets: [
        {
            label: 'My First dataset',
            backgroundColor: '#0097d4',
            borderColor: '#0072a0',
            borderWidth: 1,
            hoverBackgroundColor: '#0072a0',
            hoverBorderColor: '#0072a0',
            data: socialMediaList.Numbers
        }
    ]
};
const data4 = {
    labels: socialMediaList.Names,
    datasets: [{
        data: socialMediaList.Numbers,
        backgroundColor: [
            '#58508d',
            '#bc5090',
            '#ff6361',
            '#003f5c'

        ],
        hoverBackgroundColor: [
            '#58508d',
            '#bc5090',
            '#ff6361',
            '#003f5c'
        ]
    }]
};

export class Home extends Component {
    static displayName = Home.name;
  render () {
      return (
          <div class="row">
              <div class="col-xs-12 col-sm-6 col-md-6">
                  <div class="shadow-lg p-3 mb-5 bg-white rounded">
                      <h2>Bar Example</h2>
                      <Bar
                          data={data}
                          options={{
                              maintainAspectRatio: true
                          }}
                      />
                  </div>
                  <div class="shadow-lg p-3 mb-5 bg-white rounded">
                      <h2>Line Example</h2>
                      <Line data={data1} />
                  </div>
              </div>
              <div class="col-xs-12 col-sm-5  col-md-6">
                  <div class="shadow-lg p-3 mb-5 bg-white rounded">
                      <h2>Doughnut Example</h2>
                      <Doughnut data={data2} />
                  </div>
                  <div class="shadow-lg p-3 mb-5 bg-white rounded">
                      <h2>Horizontal Bar Example</h2>
                      <HorizontalBar data={data3} />
                  </div>
                  
                      
              </div>
              <div class="col-xs-12 col-sm-5  col-md-6">
                  <div class="shadow-lg p-3 mb-5 bg-white rounded">
                      <h2>Pie Example</h2>
                      <Pie data={data4} />
                  </div>
              </div>
    
      </div>
    );
  }
}
